---
title: 'Self-amplifying networks responsible for the origins of life'
subtitle: ''
summary: 'Are some special mechanisms behind why life uses the biomolecules it does? How can we search for them?'
authors:
- admin
tags:
- prebiotic-chemistry
categories: []
date: '2022-02-04T00:00:00Z'
featured: false
draft: false

projects: []

image:
    caption: 'A chemical reaction network can be represented as a graph of interconnected nodes. Picture: [Graph-Tool](https://graph-tool.skewed.de/)'
    focal_point: ""
    preview_only: false
---

I'll write stuff here