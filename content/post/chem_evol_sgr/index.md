---
title: 'Chemical enrichment history of the Sagittarius dwarf galaxy'
subtitle: 'An excursion into the past of our nearest Milky Way satellite using starlight.'
summary: 'How prolific was the heavy element production in Sagittarius compared to the Milky Way? What do the differences teach us?'
authors:
- admin
tags:
- astronomy
categories: []
date: '2022-02-04T00:00:00Z'
featured: false
draft: false

projects: []

image:
    caption: "An artist's conception of the tidally disrupting Sagittarius dwarf galaxy with its four star trails. (Image credit: Amanda Smith, Institute of Astronomy, University of Cambridge)"
    focal_point: ""
    preview_only: false
---

I'll write stuff here