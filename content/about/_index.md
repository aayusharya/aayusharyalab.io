---
title: "More about me"
---


I grew up in [Ladnu, Rajasthan](https://en.wikipedia.org/wiki/Ladnu), and the nearby village Mangalpura in the northwest of India. I'm currently a first-year master's student at the [Johannes-Gutenberg University of Mainz](https://www.uni-mainz.de/) in Germany. Prior to coming here, I attended [Lovely Professional University](https://www.lpu.in/) (yes, that's a real name) where I obtained my bachelor's. As someone from a middle-class Indian family, it was life-changing for me when I got [funding](https://www.studies.fb08.uni-mainz.de/physics/prospective-students/excellence-track/) to pursue my master's at [JGU Mainz](https://www.uni-mainz.de/en/) in Germany.

I'm a very outgoing person and I blabber a lot when I get excited --- I hope you won't mind that. If you insist, internet tests show that I'm an ENFJ-T, in terms of MBTI.  Personally, I don't think MBTI does a fair job of classifying people's personalities --- we all lie on a near-continuum, in a very high dimensional distribution of independent traits (innate, and acquired).

I'm enthusiastic when it comes intellectually involved discussions, not just in science. In recent years, I've also started to understand more of politics, society, and philosophy. That one day I would get interested in understanding how politics works is ironical because I definitely hated it in school.

When I was 11, I became interested in figuring out how video games were designed, thanks to my obsession with some that I used to play myself. Tinkering and learning on my own, I learnt how to teach myself to code. Thanks to the online community I was in, I also picked up [graphic designing](../#gallery) as a hobby. Until I finished school I used to think that I wanted to become a computer scientist when I grew up. Things took a turn afterwards and I have never looked back after getting into physics and astronomy, where I get to write code in addition to everything else.

I like mentoring, giving talks on a subject, and thinking of ways to creatively express myself.

On this website, you can learn about the [academic research](../#posts) I did mostly as an undergraduate, find a list of useful links and [resources](../resources/) containing a variety of things ranging from interesting articles to a collection of opportunities and even lecture notes.

### Reisen und Wandern
In the summer of 2022, I _discovered_ rather, that I enjoyed seeing new places, museums and wanted to see more of the world. I love nature, the feeling of strolling in the woods like a lost adventurer, and hiking. 

### Books
Ah, these mystical things. I'm not very good at finishing them!

.. They look fancy on my shelf though.
