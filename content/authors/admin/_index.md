---
# Display name
name: "Aayush Arya"
avatar_image: "aayush_gothenburg_1.jpg"
# Username (this should match the folder name)
authors:
- admin
# resume download button
btn:
- url : "files/Aayush_Arya_CV.pdf"
  label : "Download CV"

# Is this the primary user of the site?
superuser: true

# Role/position
role: PhD Fellow

# Organizations/Affiliations
organizations:
- name: Niels Bohr Institute, University of Copenhagen
  url: "https://cosmicdawn.dk/staff/aayush-arya/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include using spectroscopy of atoms in the laboratory, and in astronomical sources.

# Should the user's education and interests be displayed?
display_education: False

interests:
- Kilonovae
- Nucleosynthesis
- Spectroscopy
- Chemical Evolution, Galactic Archaeology

education:
  courses:
  - course: PhD in Astrophysics
    institution: University of Copenhagen
  - course: MSc in Physics (Excellence Track)
    institution: Johannes Gutenberg Universität Mainz
    year: 2024
  - course: BSc in Physics
    institution: Lovely Professional University
    year: 2022

# Social/academia Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:aayusharya71@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=7MhBMf0AAAAJ&hl=en
- icon: github
  icon_pack: fab
  link: https://github.com/cartilage-ftw
- icon: linkedin
  link: https://in.linkedin.com/in/aayush-arya
  icon_pack: fab
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0001-6782-407X
#- icon: researchgate
#  link: https://www.researchgate.net/profile/Aayush-Arya-2
#  icon_pack: ai
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/cartilage_ftw
- icon: facebook
  link: https://facebook.com/aayush.arya8
  icon_pack: fab
- icon: instagram
  link: https://instagram.com/cartilage_ftw
  icon_pack: fab
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
#- icon: cv
#  icon_pack: ai
#  link: files/Aayush_Arya_CV.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---

I'm interested in the question: "Where do the elements in our periodic table come from?". I have been involved in this problem from many facets. As a master's student I studied the atoms and nuclei of heavy elements [in the laboratory](https://lrc-project.eu/). As a PhD student at [DAWN](https://cosmicdawn.dk/) I study the spectra of certain explosions---known as kilonovae---to directly detect which heavy elements are synthesized in these violent events.

What do I like apart from science? Good question! My [Personal Info](about/) page has some clues.

Feel free to reach out to me via email or my social media handles. 
