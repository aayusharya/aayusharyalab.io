+++
# Gallery section using the Blank widget and Gallery element (shortcode).
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 66  # Order that this section will appear.

title = "Design Gallery"
subtitle = ""
+++
In my pre-teen and early teenage years, I used to design graphics (mainly, photomanipulations) as a hobby. My work can be found on my DeviantArt accounts [a-cart](https://www.deviantart.com/a-cart) and [FortNightAtmo](https://www.deviantart.com/FortNightAtmo). The following is a small selection of the work, which is almost a decade old now:
{{< gallery >}}