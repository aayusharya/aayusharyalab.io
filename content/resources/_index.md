---
title: "Resources"

---
**Note**: This page is still under construction. The list is meant to be longer ;)

### Useful Reading & Advice
Some of these are intended for graduate students, but if you're ambitious, it's never too early to be prepared
* [Stearns Lab Advice](https://stearnslab.yale.edu/modest-advice): I find this to be a better and more honest piece of advice than any other I've read. While others talk of Hamming's advice, I think I agree with most of [Raghu's remarks](https://raghumahajan.wordpress.com/2017/08/23/advice-to-researchers-by-hamming/) on that, which also indirectly points out flaws in academia.
* [Ten Simple Rules for Graduate Students](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.0030229): several excellent points. One key advice is to select a mentor, project and lab carefully. Prioritize your happiness and not the prestige.
* [Whitesides' Group: Writing a Paper](https://onlinelibrary.wiley.com/doi/epdf/10.1002/adma.200400767): My first internship advisor showed us this paper. _Actually_ incorporating this advice can help one organize the progression of their research better. I really liked how Whitesides blended core ideas of scientific thinking itself with the writing process.
	* **Personal Tip**: if _no one_ else in your project is doing this, just do it at your level. Also, make sure you understand every technical detail in your article at least superficially, otherwise hitting a wall while editing (because you don't know what to rewrite) will be common --- I say this from experience.

## Opportunities & Possible Extracurriculars
### For high school students
Having had little to no academic guidance at all until I graduated high school, I never knew about some of the things that one could actually do while in school.
* Math circles: they like to nurture students for higher math. For kids in Bangalore, [ICTS-NIAS Math Circle](https://www.icts.res.in/outreach/maths-circle). Sadly, I think there are not many in India. Maybe you could consider founding one?
* National/International Olympiads: apart from physics, math, chemistry ones, those for [astronomy & astrophysics](https://www.ioaastrophysics.org/), [informatics](https://ioinformatics.org/), linguistics, etc. also exist. If you're surprised, you're not the only one --- I didn't know of them until 12th grade.
* Science fairs: such as the Google Science Fair, the Intel ISEF. In India, one participates in the [IRIS national fair](http://irisnationalfair.org) to then advance to ISEF.

## Notes & Textbooks
In my first semester itself, I think I worked obsessively to figure out which textbooks, courses and skills I would need (and in what sequence) to learn everything I wanted. Sometimes lecture notes are more concise and practically readable than lengthy books. The following contains a selection of those that I found to be excellent. (Caution: Please don't assume that I *actually* understand everything in there).

### Astronomy & Physics
* Barbara Ryden
	* [Stellar, Galactic, and Extragalactic Astrophysics](https://www.astronomy.ohio-state.edu/ryden.1/ast292.html): Her notes are very clear and very accessible (and I liked her occassional jokes). I actually liked these notes more than the textbook of Kartunnen et al. Only thing I hated was that sometimes the approximations were annoying (no doubt we're talking astronomy, right?).
		* She has her own textbook "Foundations of Astrophysics" too, but there was no low price edition for me to buy. As an aside, I think [Zeilik-Gregory](https://books.google.co.in/books/about/Introductory_Astronomy_Astrophysics.html?id=iH7vAAAAMAAJ&redir_esc=y) is a fine book.
	* [Radiative Processes](https://www.astronomy.ohio-state.edu/ryden.1/ast822.html): I have not read much of these but they look more advanced, yet accessible.
* Jo Bovy (UTorronto) has written a textbook "[Dynamics and Astrophysics of Galaxies](https://galaxiesbook.org/)" freely available online --- he succeeded in making it less intimidating than the canonical Binney-Tremaine. It also includes Python code examples.
* Alan G. Barr (Oxford)
	* [Subatomic Physics](http://www-pnp.physics.ox.ac.uk/~barra/teaching/subatomic.pdf): also known as "Nuclear and Particle Physics".
* Sunil Golwala (Caltech)
	* [Non-accelerator experimental particle physics](https://sites.astro.caltech.edu/~golwala/ph135c/index.html): I really like that he *intentionally* made this course to cover what's usually not taught in conventional particle physics courses. One day..

For certain subjects, I think good, concise textbooks already exist.
* Steve Simon's "Oxford Solid State Basics" book goes well along with his [video lectures](https://youtube.com/playlist?list=PLaNkJORnlhZnC6E3z1-i7WERkferhQDzq). He wrote it with a physicist's perspective more so than some other authors (his presentation actually got me interested in the subject). Instead of starting with crystal structure right away, he introduces things such as transport of electrons in metals, atomic chain models, etc. His definitions are also stricter and more clearly defined than Kittel.
* Concepts in Thermal Physics by Blundell & Blundell: covers both thermodynamics and introductory statistical physics (including some quantum statistics) in a clear and concise way covering a lot of topics. I really liked how he showed that Boltzmann distributions arise naturally when you have a canonical ensemble. I wish it had more exercises though.
	* Still, every book has parts that aren't as well explained. And yes, the authors are a couple.
### Requisite Math for Physicists
I felt overwhelmed how much math one would need --- taking math department courses for everything seemed impossible. I still wanted to have some understanding of the techniques involved (and I still do not understand so much of the content). As for a book, I think the Riley-Hobson-Bence mathematical methods book (Cambridge University Press) is great, and for vector calculus, you can solely rely on their chapter on it.
* Andre Lukas (Oxford)
	* [Vectors & Matrices (Linear Algebra)](http://www-thphys.physics.ox.ac.uk/people/AndreLukas/V&M/V%26Mweb/index.html): A great balance of mathematical rigour (for my taste) and selection of topics useful for undergraduate physicists. Instead of computing matrix products, he uses a more abstract approach, starting with vector spaces.
	* [Mathematical Methods](http://www-thphys.physics.ox.ac.uk/people/AndreLukas/MathMeth/): Covers inner product spaces, Fourier series & transforms, orthogonal polynomials, The exposition is beautiful --- as one example, he explains how orthogonal polynomials form a basis for the space $\mathbb{L}^2$.
* Alexander Schekochihin (Oxford)
	* [Ordinary Differential Equations](http://www-thphys.physics.ox.ac.uk/people/AlexanderSchekochihin/ODE/2018/ODELectureNotes.pdf): a lot of figures, intuition, and enough rigour. I really hope to read thes one day myself.
	* I also hope that one day I can properly remember how to spell his name right (sorry Prof. Schekochihin).
* Y.D. Chong (NTU, Singapore)
	* [Complex Methods for the Sciences](http://www1.spms.ntu.edu.sg/~ydchong/teaching.html): the title is apt; it's complex analysis without much rigour. Really, if you need a *practical* introduction and like me (can't take Analysis courses) then this should be useful.

A more extensive list of notes and books that I compiled long ago can be found [here](https://docs.google.com/document/d/1c-aNAI5RbB1T_RFiy-nqjZR_InN7kiWJ39JJHL19H-8/edit?usp=sharing).  One thing thing I did understand is though: considering the depth of knowledge reflected in the notes of the authors, the true masters definitely master the subjects they need to understand.